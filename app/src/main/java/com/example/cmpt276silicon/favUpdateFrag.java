package com.example.cmpt276silicon;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.cmpt276silicon.RestaurantModels.RestaurantList;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatDialogFragment;

public class favUpdateFrag extends AppCompatDialogFragment {

    TextView myTextView;
    TableLayout tableLayout;
    RestaurantList restaurantList;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.favupdate_layout, null);
        ArrayList<String> nameList = getArguments().getStringArrayList("nameList");
        ArrayList<String> inspectionList = getArguments().getStringArrayList("inspectionList");
        ArrayList<String> hazardList = getArguments().getStringArrayList("hazardList");
        int size = getArguments().getInt("size");
        restaurantList = RestaurantList.getInstance(getActivity());
        tableLayout = v.findViewById(R.id.tableLayoutFav);


        for (int i = 0; i < size; i++) {

            TableRow newRow = new TableRow(getContext());

            if (i % 2 == 0) {
                newRow.setBackgroundColor(Color.parseColor("#ede9e8"));
            } else {
                newRow.setBackgroundColor(Color.parseColor("#d6d1d0"));
            }

            // TextView for RestaurantName
            TextView newTextView = new TextView(getContext());
            newTextView.setText("   " + nameList.get(i) + "\n   " + inspectionList.get(i) + "\n   " + hazardList.get(i));

            newRow.addView(newTextView);
            tableLayout.addView(newRow);

        }






        // Create a button listener
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Don't need to do anything
            }
        };
        String title = getString(R.string.title_favorites_frag);
        // Build the alert dialog
        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setView(v)
                .setPositiveButton("Done", listener)
                .create();
    }


}
