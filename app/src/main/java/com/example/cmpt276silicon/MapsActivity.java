package com.example.cmpt276silicon;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;

import com.example.cmpt276silicon.RestaurantModels.CSVUpdater;
import com.example.cmpt276silicon.RestaurantModels.Restaurant;
import com.example.cmpt276silicon.RestaurantModels.RestaurantList;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;
/*
 * Map Activity used for various reasons such as showing restaurant on the map,
 * searching for the desired restaurant, user location etc
 */

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener{

    public GoogleMap mMap;
    RestaurantList restaurantList;
    int restaurantID, counter;
    int currentResId;
    LatLng restaurantPos;
    String restaurantTitle;
    ProgressBar pb;
    ImageButton buttonHome, buttonList, buttonFollow;
    Toolbar toolbar;
    LocationManager locationManager;
    private static final int REQUEST_LOCATION = 1;
    double latitude, longitude;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private Timer timer;
    public Marker myMarker;
    boolean following = false;
    final private Activity context = this;
    private Marker mMarker;
    LinkedList<Restaurant> rListOld;
    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            //showShortToast(latitude+" "+longitude);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            showShortToast(getString(R.string.please_turn_location_service_on));
        }

        @Override
        public void onProviderEnabled(String provider) {
            //nothing
        }

        @Override
        public void onProviderDisabled(String provider) {
            showShortToast(getString(R.string.please_turn_location_service_on));
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        restaurantList = RestaurantList.getInstance(this);
        Log.d("log55", String.valueOf(restaurantList.getList().size()));
        initializeViews();
        setUpActionBar();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Make oldList used for comparing with updated list

        rListOld = restaurantList.getFilteredList();
        if(rListOld.size()==0){
            rListOld = restaurantList.getList();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        updateDialog();
        setUpImageButtonListeners();
        setUpLocationUpdate();
        //setUpClusterer();
    }

    private void setUpActionBar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.fraserhealth);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        buttonList.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, ListActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

    private void setUpImageButtonListeners(){
        buttonFollow.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (following) {
                    following = false;
                    buttonFollow.setImageResource(R.drawable.follow_off);
                    showShortToast(getString(R.string.follow_mode_off_map_activity));
                } else {
                    following = true;
                    buttonFollow.setImageResource(R.drawable.follow_on);
                    LatLng currentPos = new LatLng(latitude, longitude);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPos, 17));
                    myMarker.showInfoWindow();
                    showShortToast(getString(R.string.follow_mode_on_map_activity));
                }
            }
        });
    }

    private void updateDialog(){
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (CSVUpdater.isUpdateRequired(this) && mWifi.isConnected()) {

            final Dialog dialog = new Dialog(MapsActivity.this);
            dialog.setContentView(R.layout.update_layout);
            dialog.show();
            Button okButton = dialog.findViewById(R.id.buttonOk);
            Button cancelButton = dialog.findViewById(R.id.buttonCancel);
            pb = dialog.findViewById(R.id.progressBar);

            dialog.setOnDismissListener(new Dialog.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    LinkedList<Restaurant> rList = restaurantList.getList();
                    final ListIterator itrOld = rList.listIterator();
                    List<Restaurant> oldFavourites = new ArrayList<>();
                    while (itrOld.hasNext()) {

                        Restaurant current = (Restaurant) itrOld.next();

                        if (current.isFavorited()) {
                            oldFavourites.add(current);
                        }

                    }
                    Log.d("tag0", "restaurant");
                    RestaurantList.PopulateFromInternalCSV(restaurantList, context);
                    Log.d("tag0", Integer.toString(restaurantList.getList().size()));
                    Log.d("tag0", Integer.toString(restaurantList.getCount()));
                    if (restaurantList.getList().size() == 0) {
                        restaurantList.PopulateFromCSV(restaurantList);
                        Log.d("hashtag","here");
                    }
                    restaurantList.getListContaining("", 0, 0, Integer.MAX_VALUE, false, false);
                    updatePins();



                    for (int k = 0; k < oldFavourites.size(); k++) {
                        Log.d("favTag: ", "List - " + oldFavourites.get(k).getRestaurantName());
                    }
                    ArrayList<String> restaurantNameList = new ArrayList<String>();
                    ArrayList<String> hazardRatingList = new ArrayList<String>();
                    ArrayList<String> inspectionList = new ArrayList<String>();
                    int passSize = 0;
                    Log.d("favTag: ", "favList SIZE = " + String.valueOf(oldFavourites.size()));
                    for (int i = 0; i < oldFavourites.size(); i++) {
                        Log.d("favTag: ", "ROUND = " + String.valueOf(i) + " Beginning favList iterations to compare with new data");
                        for (int j = 0; j < restaurantList.getList().size(); j++) {

                            Restaurant current = restaurantList.getList().get(j);
                            Log.d("favTag: ", "Comparing TrackingID of" + oldFavourites.get(i).getRestaurantName() + " and " + current.getRestaurantName());
                            if (oldFavourites.get(i).getTrackingKey().equals(current.getTrackingKey())) {
                                    Log.d("favTag: ", "TRACKING ID MATCH!");
                                if (oldFavourites.get(i).getNumInspections() != current.getNumInspections()) {
                                    Log.d("favTag: ", "FAV UPDATED!");
                                    restaurantNameList.add(oldFavourites.get(i).getRestaurantName());
                                    inspectionList.add(current.getFullInspectionDate(0));
                                    hazardRatingList.add(current.getHazardRating(0));
                                    passSize++;
                                }

                            }

                        }

                    }
                    Log.d("favTag: ", "Main loop has FINISHED");

                    if (passSize > 0) {
                        FragmentManager manager = getSupportFragmentManager();
                        favUpdateFrag dialogFav = new favUpdateFrag();
                        Bundle args = new Bundle();
                        args.putStringArrayList("nameList", restaurantNameList);
                        args.putStringArrayList("inspectionList", inspectionList);
                        args.putStringArrayList("hazardList", hazardRatingList);

                        args.putInt("size", passSize);
                        dialogFav.setArguments(args);
                        dialogFav.show(manager, "favUpdateFrag");
                    }

                    following = false;
                }
            });

            okButton.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pb.setVisibility(View.VISIBLE);
                    CSVUpdater.updateCSV(context);
                    Log.d("tag0","ok button pressed");
                    progAnimation(dialog);
                }
            });
            cancelButton.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }



    private void OnGPS() {
        // Citation: https://www.tutorialspoint.com/how-to-get-current-location-latitude-and-longitude-in-android

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.enable_gps_map_activity).setCancelable(false).setPositiveButton(R.string.yes_map_activity, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton(R.string.no_map_activity, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private Location getLocation() {
        // Citation: https://stackoverflow.com/questions/15997079/getlastknownlocation-always-return-null-after-i-re-install-the-apk-file-via-ecli

        Location location = null;
        try {
            if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                showShortToast(getString(R.string.no_network_available_turn_on_location_services_map_activity));
                myMarker.setVisible(false);
                following = false;
            } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                myMarker.setVisible(true);
                OnGPS();
            } else {
                myMarker.setVisible(true);
                if (ActivityCompat.checkSelfPermission(
                        MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                }

                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        (long) 100, (float) 1, locationListener);           //update min 0.1 second and one meter change

                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    LatLng firstPos = new LatLng(latitude, longitude);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }


    private void updatePins() {

        mMap.clear();
        LinkedList<Restaurant> rList;
        if(restaurantList.hasFilteredList()) {
             rList= restaurantList.getFilteredList();
        }else{
            rList = restaurantList.getList();
        }
        final ListIterator itr = rList.listIterator();
        while (itr.hasNext()) {
            final int index = itr.nextIndex();
            Restaurant current = (Restaurant) itr.next();

            restaurantTitle = current.getRestaurantName();
            String resAddress = current.getAddress();

            String hazardLevel = current.getHazardRating(0);


            String resInfo = getString(R.string.address_map_activity) + resAddress + "    " + getString(R.string.hazard_level_map_activity) + current.getHazardRating(0);
            restaurantPos = new LatLng(current.getLatitude(), current.getLongitude());

            MarkerOptions options;

            if (current.getHazardRating(0).equals("Low")) {
                options = new MarkerOptions().position(restaurantPos).title(restaurantTitle).snippet(resInfo).icon(BitmapDescriptorFactory.fromResource(R.drawable.hazard_low_new));
            } else if (current.getHazardRating(0).equals("Moderate")) {
                options = new MarkerOptions().position(restaurantPos).title(restaurantTitle).snippet(resInfo).icon(BitmapDescriptorFactory.fromResource(R.drawable.hazard_mid_new));
            } else if (current.getHazardRating(0).equals("High")) {
                options = new MarkerOptions().position(restaurantPos).title(restaurantTitle).snippet(resInfo).icon(BitmapDescriptorFactory.fromResource(R.drawable.hazard_high_new));
            } else {
                options = new MarkerOptions().position(restaurantPos).title(restaurantTitle).snippet(resInfo).icon(BitmapDescriptorFactory.fromResource(R.drawable.no_violation_new));
            }

            mMarker = mMap.addMarker(options);

            MarkerTag yourMarkerTag = new MarkerTag();
            yourMarkerTag.setInd(index);
            yourMarkerTag.setCount(0);

            mMarker.setTag(yourMarkerTag);

            currentResId =restaurantID;
        }

        LatLng currentPos = new LatLng(latitude, longitude);
        myMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).position(currentPos).title(getString(R.string.you_are_here_map_activity)));
        myMarker.setPosition(currentPos);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPos, 15));

    }





    @Override
    public void onMapReady(GoogleMap googleMap) {
        Bundle extras = getIntent().getExtras();
        System.out.println(getString(R.string.got_here_map_activity));
        latitude = 49.104431;
        longitude = -122.801014;
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
        updatePins();

        if (extras != null) {
            restaurantID = extras.getInt("restaurantID");
            Restaurant restaurant = restaurantList.getRestaurant(restaurantID);
            restaurantTitle = restaurant.getRestaurantName();
            restaurantPos  = new LatLng(restaurant.getLatitude() , restaurantList.getRestaurant(restaurantID).getLongitude());
            String resAddress = restaurant.getAddress();
            String hazardRating = restaurant.getHazardRating(0);
            String resInfo = getString(R.string.address_map_activity) + resAddress + "    " + getString(R.string.hazard_level_map_activity) + hazardRating;
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(restaurantPos,15));
            MarkerOptions options;
            if (hazardRating.equals("Low")) {
                options = new MarkerOptions().position(restaurantPos).title(restaurantTitle).snippet(resInfo).icon(BitmapDescriptorFactory.fromResource(R.drawable.hazard_low_new));
            } else if (hazardRating.equals("Moderate")) {
                options = new MarkerOptions().position(restaurantPos).title(restaurantTitle).snippet(resInfo).icon(BitmapDescriptorFactory.fromResource(R.drawable.hazard_mid_new));
            } else if (hazardRating.equals("High")) {
                options = new MarkerOptions().position(restaurantPos).title(restaurantTitle).snippet(resInfo).icon(BitmapDescriptorFactory.fromResource(R.drawable.hazard_high_new));
            } else {
                options = new MarkerOptions().position(restaurantPos).title(restaurantTitle).snippet(resInfo).icon(BitmapDescriptorFactory.fromResource(R.drawable.no_violation_new));
            }
            mMarker = mMap.addMarker(options);

            MarkerTag yourMarkerTag = new MarkerTag();
            yourMarkerTag.setInd(restaurantID);
            yourMarkerTag.setCount(0);
            mMarker.setTag(yourMarkerTag);

            mMarker.showInfoWindow();

        } else {

            Location ret = getLocation();
            LatLng currentPos = new LatLng(latitude, longitude);

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPos,15));
            if(ret==null) {
                myMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).position(currentPos).title(getString(R.string.you_are_here_map_activity)));
            }
        }
        mMap.setOnInfoWindowClickListener(this);
    }

    public void progAnimation(final Dialog dialog) {
        // For loading bar animation
        // Citation: https://www.youtube.com/watch?v=K5bFv_WDjVY

        counter = 0;
        pb.setProgress(counter);

        final Timer t = new Timer();
        TimerTask tt = new TimerTask() {
            @Override
            public void run() {
                counter++;
                pb.setProgress(counter);
                if (counter >= 100) {
                    pb.setProgress(100);
                    dialog.dismiss();


                    t.cancel();

                }
                if(counter<95&&!CSVUpdater.isUpdating()){
                    counter = 95;
                }
            }
        };

        t.schedule(tt, 0, 80);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
                builder.setTitle(R.string.title_map_activity);

// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                final View dialogView = getLayoutInflater().inflate(R.layout.filter_dialog,null);
                builder.setView(dialogView);
                CheckBox checkBox = (CheckBox) dialogView.findViewById(R.id.checkboxFavorite);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        ToggleButton favoritedToggle = dialogView.findViewById(R.id.favoritedToggle);
                        if (isChecked) {
                            favoritedToggle.setEnabled(true);
                        }
                        else{
                            favoritedToggle.setEnabled(false);
                        }
                    }
                });
// Set up the buttons
                builder.setPositiveButton(R.string.ok_map_activity, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String inputSubstring = ((EditText)dialogView.findViewById(R.id.inputSubstring)).getText().toString();
                        String inputHazardRating = ((EditText)dialogView.findViewById(R.id.inputHazardRating)).getText().toString();
                        String inputMinCritVio = ((EditText)dialogView.findViewById(R.id.inputMinCritVio)).getText().toString();
                        String inputMaxCritVio = ((EditText)dialogView.findViewById(R.id.inputMaxCritVio)).getText().toString();
                        ToggleButton favoritedToggle = dialogView.findViewById(R.id.favoritedToggle);
                        CheckBox favoritesCheckBox = dialogView.findViewById(R.id.checkboxFavorite);
                        String substring = inputSubstring.trim();
                        int hazardRating = parseWithDefault(inputHazardRating.trim(), 0);
                        int minCritVio = parseWithDefault(inputMinCritVio.trim(), -1);
                        int maxCritVio = parseWithDefault(inputMaxCritVio.trim(), Integer.MAX_VALUE);
                        boolean usingFavorites = favoritesCheckBox.isChecked();
                        boolean favorited = favoritedToggle.isChecked();

                        if(hazardRating>=0&&hazardRating<=3) {
                            restaurantList.getListContaining(substring,hazardRating,minCritVio,maxCritVio,favorited,usingFavorites);
                            updatePins();
                            dialog.dismiss();
                        }else{
                            showShortToast(getString(R.string.invalid_hazard_rating1_map_activity));
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel_map_activity, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

                return true;
            case R.id.item2:
                showShortToast(getString(R.string.setting_not_implemented_here));
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initializeViews() {
        toolbar = findViewById(R.id.toolbar1);
        buttonHome = findViewById(R.id.buttonHome);
        buttonList = findViewById(R.id.buttonList);
        buttonFollow = findViewById(R.id.buttonFollow);
    }

    private void showShortToast(String str) {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    private void setUpLocationUpdate(){
        final Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                LatLngBounds currentScreen = mMap.getProjection().getVisibleRegion().latLngBounds;
                getLocation();
                LatLng currentPos = new LatLng(latitude, longitude);

                myMarker.setPosition(currentPos);

                if (following) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPos, 17));
                }


                handler.postDelayed(this, 1000);
            }
        };

        handler.postDelayed(r, 1000);
    }

    public class MarkerTag {


        private int ind;
        private int count;

        public MarkerTag() {

        }

        public int getInd() {
            return ind;
        }

        public void setInd(int index) {
            this.ind = index;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int c) {
            this.count = c;
        }
    }

    @Override
    public void onInfoWindowClick(final Marker marker) {

        Log.d("tag10","here");
        if (null != marker.getTag()) {
            Log.d("tag10","1");
            if (marker.getTag() instanceof MarkerTag) {
                Log.d("tag10","2");
                MarkerTag yourMarkerTag = ((MarkerTag) marker.getTag());
                currentResId = yourMarkerTag.getInd();
                Intent intent = new Intent(MapsActivity.this, RestaurantActivity.class);
                intent.putExtra("restaurantID", currentResId);
                startActivity(intent);
            }
        }

    }

    public static int parseWithDefault(String s, int defaultVal) {
        return s.matches("-?\\d+") ? Integer.parseInt(s) : defaultVal;
    }

}
