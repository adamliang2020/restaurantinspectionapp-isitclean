package com.example.cmpt276silicon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

/*
 * Search Activity
 */

public class SearchActivity extends AppCompatActivity {

    ImageView buttonHome, buttonFav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

    }

    private void initalizeViews() {
        // Connects all of the views' IDs to class variables
        buttonHome = findViewById(R.id.buttonHome);
        buttonFav = findViewById(R.id.buttonList);
        //buttonSettings = findViewById(R.id.buttonSettings);
    }

    private void setupHomeButton() {
        buttonHome.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SearchActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupFavButton() {
        buttonFav.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SearchActivity.this, FavoritesActivity.class);
                startActivity(intent);
            }
        });
    }

    /*
    private void setupSettingsButton() {
        buttonSettings.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SearchActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

     */

}
