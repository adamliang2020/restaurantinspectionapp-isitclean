package com.example.cmpt276silicon;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.cmpt276silicon.RestaurantModels.Restaurant;
import com.example.cmpt276silicon.RestaurantModels.RestaurantList;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.ListIterator;

/*
 * Main Activity for showing logo, name of the restaurant, # issues found (sum of critical and non-critical issues)
 * hazard rating symbol, and updated is the date when last inspection happened
 */

public class ListActivity extends AppCompatActivity {

    RestaurantList restaurantList;
    TableLayout tableLayout;
    Toolbar toolbar;
    ImageButton buttonHome;
    CountDownTimer mCountDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        restaurantList = RestaurantList.getInstance(this);
        Log.d("log66", String.valueOf(restaurantList.getList().size()));
        initalizeViews();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.fraserhealth);
        setupHomeButton();

        fillTableOverTime("", 0, -1, Integer.MAX_VALUE,false);
    }
    @Override
    public void onRestart()
    {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1: //https://stackoverflow.com/questions/10903754/input-text-dialog-android
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.title_list_activity);

                final View dialogView = getLayoutInflater().inflate(R.layout.filter_dialog,null);
                builder.setView(dialogView);
                CheckBox checkBox = (CheckBox) dialogView.findViewById(R.id.checkboxFavorite);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        ToggleButton favoritedToggle = dialogView.findViewById(R.id.favoritedToggle);
                        if (isChecked) {
                            favoritedToggle.setEnabled(true);
                        }
                        else{
                            favoritedToggle.setEnabled(false);
                        }
                    }
                });

// Set up the buttons
                builder.setPositiveButton(R.string.ok_list_activity, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCountDownTimer.cancel();
                        tableLayout.removeAllViews();
                        String inputSubstring = ((EditText)dialogView.findViewById(R.id.inputSubstring)).getText().toString();
                        String inputHazardRating = ((EditText)dialogView.findViewById(R.id.inputHazardRating)).getText().toString();
                        String inputMinCritVio = ((EditText)dialogView.findViewById(R.id.inputMinCritVio)).getText().toString();
                        String inputMaxCritVio = ((EditText)dialogView.findViewById(R.id.inputMaxCritVio)).getText().toString();
                        ToggleButton favoritedToggle = dialogView.findViewById(R.id.favoritedToggle);
                        CheckBox favoritesCheckBox = dialogView.findViewById(R.id.checkboxFavorite);
                        String substring = inputSubstring.trim();
                        int hazardRating = parseWithDefault(inputHazardRating.trim(), 0);
                        int minCritVio = parseWithDefault(inputMinCritVio.trim(), -1);
                        int maxCritVio = parseWithDefault(inputMaxCritVio.trim(), Integer.MAX_VALUE);
                        boolean usingFavorites = favoritesCheckBox.isChecked();
                        boolean favorited = favoritedToggle.isChecked();

                        if(hazardRating>=0&&hazardRating<=3) {
                            restaurantList.getListContaining(substring, hazardRating, minCritVio, maxCritVio, favorited,usingFavorites);
                            fillTableOverTime(substring, hazardRating, minCritVio, maxCritVio, favorited);
                            dialog.dismiss();
                        }else{
                            showShortToast(getString(R.string.invalid_hazard_rating_list_activity));
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel_list_activity, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

                return true;
            case R.id.item2:
                Toast.makeText(this, R.string.setting_not_implemented_string, Toast.LENGTH_SHORT).show();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void fillTableOverTime(String substring, int hazardRating, int minCritVio, int maxCritVio, boolean usingFavorites) {
        LinkedList<Restaurant> rList;
        if(!restaurantList.hasFilteredList()) {
            Log.d("chef4", "chef4");
            rList = restaurantList.getListContaining(substring, hazardRating, minCritVio, maxCritVio, false, usingFavorites);
        }else{
            Log.d("chef5", "chef5");
            rList = restaurantList.getFilteredList();
        }
        Log.d("chef3", String.valueOf(rList.size()));
        final ListIterator itr = rList.listIterator();

        TableRow init = new TableRow(this);

        TextView col1 = new TextView(this);
        col1.setTypeface(null, Typeface.BOLD);
        col1.setText(R.string.logo);
        TextView col2 = new TextView(this);
        col2.setTypeface(null, Typeface.BOLD);
        col2.setText(R.string.name);
        TextView col3 = new TextView(this);
        col3.setTypeface(null, Typeface.BOLD);
        col3.setText(R.string.issues);
        TextView col4 = new TextView(this);
        col4.setTypeface(null, Typeface.BOLD);
        col4.setText(R.string.rating);
        TextView col5 = new TextView(this);
        col5.setTypeface(null, Typeface.BOLD);
        col5.setText(R.string.updated_list_activity);

        init.addView(col1);
        init.addView(col2);
        init.addView(col3);
        init.addView(col4);
        init.addView(col5);

        tableLayout.addView(init);
        fillTable(itr);

        mCountDownTimer = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //do nothing
            }

            @Override
            public void onFinish() {
                if(!fillTable(itr)){
                    timeInterval(itr);
                }
            }
        }.start();

    }

    private void timeInterval(final ListIterator itr){
        mCountDownTimer = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //do nothing
            }

            @Override
            public void onFinish() {
                if(!fillTable(itr)){
                    timeInterval(itr);
                }
            }
        }.start();
    }

    private boolean fillTable(ListIterator itr) {

        int loaded = 0;

        while(itr.hasNext()&&loaded<30) {
            final int index = itr.nextIndex();
            Restaurant current = (Restaurant) itr.next();

            TableRow newRow = new TableRow(this);
            newRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            newRow.setGravity(Gravity.CENTER_VERTICAL);


            if (index % 2 == 0) {
                newRow.setBackgroundColor(Color.parseColor("#ede9e8"));
            } else {
                newRow.setBackgroundColor(Color.parseColor("#d6d1d0"));
            }
            if(current.isFavorited()){
                newRow.setBackgroundColor(Color.parseColor("#E38F8F"));
            }


            ImageView logo = new ImageView(this);
            if (current.getRestaurantName().contains("A&W")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.a_and_w));
            } else if (current.getRestaurantName().contains("Domino's")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.dominos));
            } else if (current.getRestaurantName().contains("Freshii")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.freshii));
            } else if (current.getRestaurantName().contains("KFC")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.kfc));
            } else if (current.getRestaurantName().contains("McDonald's")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.mc_donalds));
            } else if (current.getRestaurantName().contains("Papa John's")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.papa_johns));
            } else if (current.getRestaurantName().contains("Panago")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.panago));
            } else if (current.getRestaurantName().contains("Pizza Hut")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.pizza_hut));
            } else if (current.getRestaurantName().contains("Real Canadian Superstore")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.real_canadian_superstore));
            } else if (current.getRestaurantName().contains("Safeway")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.safeway));
            }else if (current.getRestaurantName().contains("Starbucks")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.starbucks));
            } else if (current.getRestaurantName().contains("7-Eleven")) {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.seven_eleven));
            } else {
                logo.setImageDrawable(getResources().getDrawable(R.drawable.res_generic));
            }

            TextView resName = new TextView(this);
            resName.setText(current.getRestaurantName());


            TextView issueCount = new TextView(this);

            issueCount.setText(String.valueOf(current.getNumCritVio(0) + current.getNumNonCritVio(0)));
            issueCount.setGravity(Gravity.CENTER);

            ImageView hazardIcon = new ImageView(this);
            if (String.valueOf(current.getHazardRating(0)).equals("Low")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_low));
            } else if (current.getHazardRating(0).equals("Moderate")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_mid));
            } else if (current.getHazardRating(0).equals("High")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_high));
            } else {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.no_violation));
            }

            TextView lastInspection = new TextView(this);
            Calendar today = Calendar.getInstance();
            lastInspection.setText( current.getInspectionDate(0));

            newRow.setOnClickListener(new TableRow.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCountDownTimer.cancel();
                    Intent intent = new Intent(ListActivity.this, RestaurantActivity.class);
                    intent.putExtra("restaurantID", index);
                    startActivity(intent);
                }
            });

            resName.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            newRow.addView(logo, 100, 100);
            newRow.addView(resName, 600, 100);
            newRow.addView(issueCount);
            newRow.addView(hazardIcon, 100, 100);
            newRow.addView(lastInspection);
            tableLayout.addView(newRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

            loaded++;

        }
        Log.d("imp",loaded+" ");
        if(loaded==30) {
            return false;
        }else {
            return true;
        }
    }

    private void showShortToast(String str) {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    private void initalizeViews() {
        // Connects all of the views' IDs to local variables
        buttonHome = findViewById(R.id.buttonHome);
        tableLayout = findViewById(R.id.tableLayoutFav);
        toolbar = findViewById(R.id.toolbar1);

    }

    private void setupHomeButton() {
        buttonHome.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                mCountDownTimer.cancel();
                Intent intent = new Intent(ListActivity.this, MapsActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

    public static int parseWithDefault(String s, int defaultVal) {
        return s.matches("-?\\d+") ? Integer.parseInt(s) : defaultVal;
    }
}
