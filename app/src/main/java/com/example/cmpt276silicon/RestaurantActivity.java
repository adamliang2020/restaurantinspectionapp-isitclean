package com.example.cmpt276silicon;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.cmpt276silicon.RestaurantModels.InspectionData;
import com.example.cmpt276silicon.RestaurantModels.RestaurantList;

import java.util.LinkedList;
import java.util.ListIterator;

/*
 * Restaurant Activity for showing the details about a single restaurant
 * so user can understand the history of inspections at that restaurant.
 * It is showing the Restaurant name, Restaurant address, Restaurant GPS coords.
 * Table Layout is used to show hazard level, date, critical, and non critical issues
 */


public class RestaurantActivity extends AppCompatActivity {

    ImageButton buttonHome, buttonList, buttonFav;
    TextView textName, textAddr, textLat, textLon, textCrit;
    RestaurantList restaurantList;
    TableLayout tableLayout;
    Toolbar toolbar;
    int restaurantID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        restaurantList = RestaurantList.getInstance(this);
        initalizeViews();
        setupHomeButton();
        setupListButton();
        setupFavButton();
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setIcon(R.drawable.fraserhealth);
        fillTable();
        gpsCoords();
        Bundle extras = getIntent().getExtras();
        restaurantID = extras.getInt("restaurantID");


        if (extras != null) {
            textName.setText(restaurantList.getRestaurant(restaurantID).getRestaurantName());
            textAddr.setText(restaurantList.getRestaurant(restaurantID).getAddress());
            textLat.setText(String.valueOf(restaurantList.getRestaurant(restaurantID).getLatitude()));
            textLon.setText(String.valueOf(restaurantList.getRestaurant(restaurantID).getLongitude()));

            if(restaurantList.getRestaurant(restaurantID).isFavorited()){
                buttonFav.setBackgroundResource(R.drawable.fav2);
            }
            else{
                buttonFav.setBackgroundResource(R.drawable.fav1);
            }

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Toast.makeText(this, R.string.search_not_implemented_string, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item2:
                Toast.makeText(this, R.string.setting_not_implemented_string, Toast.LENGTH_SHORT).show();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void gpsCoords() {

        Bundle extras = getIntent().getExtras();
        restaurantID = extras.getInt("restaurantID");
        textLat = findViewById(R.id.textViewLat);
        textLon = findViewById(R.id.textViewLon);

        textLat.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RestaurantActivity.this, MapsActivity.class);
                intent.putExtra("restaurantID", restaurantID);
                startActivity(intent);
            }
        });

        textLon.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RestaurantActivity.this, MapsActivity.class);
                intent.putExtra("restaurantID", restaurantID);
                startActivity(intent);
            }
        });
    }





    public void fillTable() {
        Bundle extras = getIntent().getExtras();
        restaurantID = extras.getInt("restaurantID");
        LinkedList<InspectionData> iList = restaurantList.getRestaurant(restaurantID).getInspectionDataLinkedList();
        final ListIterator itr = iList.listIterator();
        if(itr.hasNext()==false){   //no inspections for restaurant
            Toast.makeText(getApplicationContext(), R.string.no_inspection_reataurant_activity, Toast.LENGTH_LONG).show();
            return;
        }
        while (itr.hasNext()) {

            final int index = itr.nextIndex();
            final InspectionData current = (InspectionData) itr.next();


            TableRow newRow = new TableRow(this);
            newRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            newRow.setGravity(Gravity.CENTER_VERTICAL);
            if (index % 2 == 0) {
                newRow.setBackgroundColor(Color.parseColor("#ede9e8"));
            } else {
                newRow.setBackgroundColor(Color.parseColor("#d6d1d0"));
            }


            ImageView hazardIcon = new ImageView(this);
            if (current.getHazardRating().equals("Low")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_low));
            } else if (current.getHazardRating().equals("Moderate")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_mid));
            } else if (current.getHazardRating().equals("High")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_high));
            } else {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.no_violation));
            }


            TextView inspectionDate = new TextView(this);
            inspectionDate.setText(String.valueOf(current.getInspectionDateString()));


            TextView crit = new TextView(this);
            crit.setText(String.valueOf(current.getNumCritVio()));
            crit.setGravity(Gravity.CENTER);


            TextView nonCrit = new TextView(this);
            nonCrit.setText(String.valueOf(current.getNumNonCritVio()));
            nonCrit.setGravity(Gravity.CENTER);


            newRow.setOnClickListener(new TableRow.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(RestaurantActivity.this, InspectActivity.class);
                    intent.putExtra("restaurantID", restaurantID);
                    intent.putExtra("inspectionId", index);
                    intent.putExtra("numCrit", current.getNumCritVio());
                    intent.putExtra("numNonCrit",current.getNumNonCritVio());
                    intent.putExtra("hazardRating",current.getHazardRating());

                    startActivity(intent);
                }
            });


            inspectionDate.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            newRow.addView(hazardIcon, 100, 100);
            newRow.addView(inspectionDate);
            newRow.addView(crit);
            newRow.addView(nonCrit);
            tableLayout.addView(newRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

        }
    }


    private void initalizeViews() {
        // Connects all of the views' IDs to local variables
        buttonHome = findViewById(R.id.buttonHome);
        buttonList = findViewById(R.id.buttonList);
        buttonFav = findViewById(R.id.fav_button);
        textName = findViewById(R.id.textViewName);
        textAddr = findViewById(R.id.textViewAddr);
        textLat = findViewById(R.id.textViewLat);
        textLon = findViewById(R.id.textViewLon);
        textCrit = findViewById(R.id.textViewCrit);
        tableLayout = findViewById(R.id.restaurentTableLayout);
        toolbar = findViewById(R.id.toolbar1);
    }

    private void setupFavButton() {
        buttonFav.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {

                restaurantList.toggleFavorites(restaurantID);

                if(restaurantList.getRestaurant(restaurantID).isFavorited()){
                    buttonFav.setBackgroundResource(R.drawable.fav2);
                }
                else{
                    buttonFav.setBackgroundResource(R.drawable.fav1);
                }

            }
        });
    }

    private void setupHomeButton() {
        buttonHome.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RestaurantActivity.this, MapsActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

    private void setupListButton() {
        buttonList.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RestaurantActivity.this, ListActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }
}
