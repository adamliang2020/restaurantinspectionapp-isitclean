package com.example.cmpt276silicon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

/*
 * Setting Activity
 */

public class SettingsActivity extends AppCompatActivity {

    ImageView buttonHome, buttonFav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initalizeViews();
        setupHomeButton();
        setupFavButton();

    }

    private void initalizeViews() {
        // Connects all of the views' IDs to local variables
        buttonHome = findViewById(R.id.buttonHome);
        buttonFav = findViewById(R.id.buttonList);
    }

    private void setupHomeButton() {
        buttonHome.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupFavButton() {
        buttonFav.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, FavoritesActivity.class);
                startActivity(intent);
            }
        });
    }


}
