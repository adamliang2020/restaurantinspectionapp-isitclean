package com.example.cmpt276silicon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

/*
 * Future Activity
 */

public class FavoritesActivity extends AppCompatActivity {

    ImageView buttonHome, buttonFav, buttonSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        initalizeViews();
        setupHomeButton();
        setupSettingsButton();
    }

    private void initalizeViews() {
        // Connects all of the views' IDs to local variables
        buttonHome = findViewById(R.id.buttonHome);
        buttonSettings =  findViewById(R.id.buttonSettings);
    }

    private void setupHomeButton() {
        buttonHome.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FavoritesActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupSettingsButton() {
        buttonSettings.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FavoritesActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }
}
