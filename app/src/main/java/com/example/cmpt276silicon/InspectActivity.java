package com.example.cmpt276silicon;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.cmpt276silicon.RestaurantModels.InspectionData;
import com.example.cmpt276silicon.RestaurantModels.RestaurantList;

import java.util.HashMap;

/*
 * Inspection Activity for Full date of the inspection (such as "May 12, 2019"), Inspection type (routine / follow-up)
 * # critical issues found, # non-critical issues found, Hazard level icon, hazard level in words, and colour coded,
 * Scrollable list of violations, An icon reflecting the nature of the violation (food, pest, equipment, ...),
 * A brief description of the violation which fits on one line
 */
public class InspectActivity extends AppCompatActivity {

    ImageButton buttonHome, buttonList;
    ImageView hazardIcon;
    TextView resName, insType, insDate, numCrit, numNonCrit;
    RestaurantList restaurantList;
    TableLayout tableLayout;
    Toolbar toolbar;
    private HashMap<Integer, String> vioMap = new HashMap<Integer, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspect);
        restaurantList = RestaurantList.getInstance(this);
        initalizeViews();
        setupHomeButton();
        setupListButton();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.fraserhealth);
        fillHashMap();
        fillTable();
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int restaurantID = extras.getInt("restaurantID");
            int inspectionID = extras.getInt("inspectionId");

            InspectionData myInspection = restaurantList.getRestaurant(restaurantID).getInspectionDataLinkedList().get(inspectionID);
            int critVio = myInspection.getNumCritVio();
            int nonCritVio = myInspection.getNumNonCritVio();
            String hazardRating = myInspection.getHazardRating();

            resName.setText(restaurantList.getRestaurant(restaurantID).getRestaurantName());
            insType.setText(myInspection.getInspectionType());
            insDate.setText(restaurantList.getRestaurant(restaurantID).getFullInspectionDate(inspectionID));
            numCrit.setText(String.valueOf(critVio));
            numNonCrit.setText(String.valueOf(nonCritVio));
            if (hazardRating.equals("Low")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_low));
            } else if (hazardRating.equals("Moderate")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_mid));
            } else if (hazardRating.equals("High")) {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.hazard_high));
            } else {
                hazardIcon.setImageDrawable(getResources().getDrawable(R.drawable.no_violation));
            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Toast.makeText(this, R.string.search_not_implemented_string, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item2:
                Toast.makeText(this, R.string.setting_not_implemented_string, Toast.LENGTH_SHORT).show();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void fillTable() {
        Bundle extras = getIntent().getExtras();
        int restaurantID = extras.getInt("restaurantID");
        int inspectionID = extras.getInt("inspectionId");
        InspectionData myInspection = restaurantList.getRestaurant(restaurantID).getInspectionDataLinkedList().get(inspectionID);
        if(myInspection.getNumCritVio()+myInspection.getNumNonCritVio()==0){            //if no violations in inspection
            Toast.makeText(getApplicationContext(), R.string.no_violation_inspect_activity, Toast.LENGTH_LONG).show();
            return;
        }

        String vio = String.valueOf(myInspection.getViolations());
        String []arrOfViol = vio.split("\\|");
        int len = arrOfViol.length;
        Log.d("error", Integer.toString(len));
        if (myInspection.getNumCritVio() == 0 & myInspection.getNumNonCritVio() == 0) {
            tableLayout.setVisibility(View.INVISIBLE);
        }

        for (int i = 0; i < len; i++) {
            final String currentVio = arrOfViol[i];
            int vioNum = Integer.parseInt(currentVio.substring(0, 3));
            TableRow newRow = new TableRow(this);
            newRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            newRow.setGravity(Gravity.CENTER_VERTICAL);

            newRow.setOnClickListener(new TableRow.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), currentVio, Toast.LENGTH_SHORT).show();
                }
            });

            if (i % 2 == 0) {
                newRow.setBackgroundColor(Color.parseColor("#ede9e8"));
            } else {
                newRow.setBackgroundColor(Color.parseColor("#d6d1d0"));
            }

            ImageView vioIcon = new ImageView(this);
            if (arrOfViol[i].contains("201")
                    | arrOfViol[i].contains("203")
                    | arrOfViol[i].contains("204")
                    | arrOfViol[i].contains("205")
                    | arrOfViol[i].contains("206")
                    | arrOfViol[i].contains("209")
                    | arrOfViol[i].contains("210")
                    | arrOfViol[i].contains("211")) {
                vioIcon.setImageDrawable(getResources().getDrawable(R.drawable.food_violation));
            } else if (arrOfViol[i].contains("304")
                    | arrOfViol[i].contains("305")) {
                vioIcon.setImageDrawable(getResources().getDrawable(R.drawable.pests_violation));
            } else if (arrOfViol[i].contains("301")
                    | arrOfViol[i].contains("302")
                    | arrOfViol[i].contains("303")
                    | arrOfViol[i].contains("306")
                    | arrOfViol[i].contains("313")
                    | arrOfViol[i].contains("314")
                    | arrOfViol[i].contains("401")
                    | arrOfViol[i].contains("402")
                    | arrOfViol[i].contains("403")
                    | arrOfViol[i].contains("404")) {
                vioIcon.setImageDrawable(getResources().getDrawable(R.drawable.unsanitary_violation));
            } else if (arrOfViol[i].contains("307")
                    | arrOfViol[i].contains("308")
                    | arrOfViol[i].contains("310")
                    | arrOfViol[i].contains("403")) {
                vioIcon.setImageDrawable(getResources().getDrawable(R.drawable.utensils_violation));
            } else {
                vioIcon.setImageDrawable(getResources().getDrawable(R.drawable.non_critial_violation));
            }


            TextView vioText = new TextView(this);
            vioText.setGravity(Gravity.CENTER_VERTICAL);
            vioText.setText(vioMap.get(vioNum));


            ImageView severityIcon = new ImageView(this);
            if (arrOfViol[i].contains("Not Critical")) {
                severityIcon.setImageDrawable(getResources().getDrawable(R.drawable.non_critial_violation));
            } else {
                severityIcon.setImageDrawable(getResources().getDrawable(R.drawable.critical_violation));
            }

            newRow.addView(vioIcon, 100, 100);
            newRow.addView(vioText, TableLayout.LayoutParams.WRAP_CONTENT, 100);
            newRow.addView(severityIcon, 100, 100);

            tableLayout.addView(newRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

        }

    }

    private void initalizeViews() {
        // Connects all of the views' IDs to local variables
        buttonHome = findViewById(R.id.buttonHome);
        buttonList = findViewById(R.id.buttonList);
        resName = findViewById(R.id.textViewName);
        insType = findViewById(R.id.textViewInspType);
        insDate = findViewById(R.id.textViewInspDate);
        numCrit = findViewById(R.id.textViewCrit);
        numNonCrit = findViewById(R.id.textViewNonCrit);
        hazardIcon = findViewById(R.id.imageViewHazardIcon);
        tableLayout = findViewById(R.id.inspecTable);
        toolbar = findViewById(R.id.toolbar1);

    }

    private void setupHomeButton() {
        buttonHome.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InspectActivity.this, MapsActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

    private void setupListButton() {
        buttonList.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InspectActivity.this, ListActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }

    private void fillHashMap() {
        // fills hash map with violation brief detail

        vioMap.put(101, getString(R.string.vio_101));
        vioMap.put(102, getString(R.string.vio_102));
        vioMap.put(103, getString(R.string.vio_103));
        vioMap.put(104, getString(R.string.vio_104));
        vioMap.put(201, getString(R.string.vio_201));
        vioMap.put(202, getString(R.string.vio_202));
        vioMap.put(203, getString(R.string.vio_203));
        vioMap.put(204, getString(R.string.vio_204));
        vioMap.put(205, getString(R.string.vio_205));
        vioMap.put(206, getString(R.string.vio_206));
        vioMap.put(208, getString(R.string.vio_208));
        vioMap.put(209, getString(R.string.vio_209));
        vioMap.put(210, getString(R.string.vio_210));
        vioMap.put(211, getString(R.string.vio_211));
        vioMap.put(212, getString(R.string.vio_212));
        vioMap.put(301, getString(R.string.vio_301));
        vioMap.put(302, getString(R.string.vio_302));
        vioMap.put(303, getString(R.string.vio_303));
        vioMap.put(304, getString(R.string.vio_304));
        vioMap.put(305, getString(R.string.vio_305));
        vioMap.put(306, getString(R.string.vio_306));
        vioMap.put(307, getString(R.string.vio_307));
        vioMap.put(308, getString(R.string.vio_308));
        vioMap.put(309, getString(R.string.vio_309));
        vioMap.put(310, getString(R.string.vio_310));
        vioMap.put(311, getString(R.string.vio_311));
        vioMap.put(312, getString(R.string.vio_312));
        vioMap.put(313, getString(R.string.vio_313));
        vioMap.put(314, getString(R.string.vio_314));
        vioMap.put(315, getString(R.string.vio_315));
        vioMap.put(401, getString(R.string.vio_401));
        vioMap.put(402, getString(R.string.vio_402));
        vioMap.put(403, getString(R.string.vio_403));
        vioMap.put(404, getString(R.string.vio_404));
        vioMap.put(501, getString(R.string.vio_501));
        vioMap.put(502, getString(R.string.vio_502));

    }
}
