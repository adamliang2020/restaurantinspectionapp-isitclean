package com.example.cmpt276silicon.RestaurantModels;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.ListIterator;

/*
 * Java class for Restaurant which is implementing all the set and get function
 */
public class Restaurant {
    private String address;
    private String City;
    private LinkedList<InspectionData> inspectionDataLinkedList;
    private String restaurantName;
    private double latitude;
    private double longitude;
    private String trackingKey;
    private String months[];
    private int numInspections;
    private boolean isFavorited = false;

    public Restaurant( String trackingKey, String restaurantName, String address, String City, double latitude, double longitude, LinkedList<InspectionData> inspectionDataLinkedList, int numInspections){
        this.trackingKey = trackingKey;
        this.address = address;
        this.City =City;
        this.restaurantName = restaurantName;
        this.latitude = latitude;
        this.longitude = longitude;

        if(inspectionDataLinkedList!=null) {
            this.inspectionDataLinkedList = inspectionDataLinkedList;
        }else{
            this.inspectionDataLinkedList = new LinkedList<InspectionData>();
        }
        this.numInspections = numInspections;
        months =  new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    }

    public void update(InspectionData iData){
        ListIterator itr = inspectionDataLinkedList.listIterator();
        boolean inserted = false;
        while(itr.hasNext()){
            InspectionData compare = (InspectionData)itr.next();
            if(iData.getInspectionDate().compareTo(compare.getInspectionDate())==1){
                itr.previous();
                itr.add(iData);
                inserted = true;
                break;
            }
        }
        if(!inserted){
            inspectionDataLinkedList.addLast(iData);
        }
        numInspections++;
    }

    public void setHazardRating(String hazardRating, int index) {
        this.inspectionDataLinkedList.get(index).setHazardRating(hazardRating);
    }

    public String getHazardRating(int index){
        if(numInspections==0){
            return "";
        }
        return inspectionDataLinkedList.get(index).getHazardRating();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getRestaurantName() {
        return restaurantName;
    }


    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getInspectionDate(int index) {
        if(numInspections==0){
            return "N/A";
        }
        //String dateFormat;
        Calendar date = inspectionDataLinkedList.get(index).getInspectionDate();
        long days = daysSinceInspection(index);
        if(days<10){
            return Long.toString(days)+ " days ago";
        }else if(days<365){
            return months[date.get(Calendar.MONTH)]+ " "+date.get(Calendar.DAY_OF_MONTH);
        }else{
            return months[date.get(Calendar.MONTH)]+ " "+date.get(Calendar.YEAR);
        }
        /*dateFormat = months[date.get(Calendar.MONTH)]+ " "+date.get(Calendar.DAY_OF_MONTH)+", "+date.get(Calendar.YEAR);
        return dateFormat;*/
    }

    public String getFullInspectionDate(int index){
        Calendar date = inspectionDataLinkedList.get(index).getInspectionDate();
        long days = daysSinceInspection(index);
        String dateFormat = months[date.get(Calendar.MONTH)]+ " "+date.get(Calendar.DAY_OF_MONTH)+", "+date.get(Calendar.YEAR);
        return dateFormat;
    }

    public long daysSinceInspection(int index){
        if(numInspections==0){
            return -1;
        }
        Calendar lastInspection = inspectionDataLinkedList.get(index).getInspectionDate();
        long nowMil = System.currentTimeMillis();
        long diffMil = nowMil - lastInspection.getTimeInMillis();
        long diffDays = diffMil / (24*60*60*1000);
        return diffDays;
    }

    public void setInspectionDate(int index, Calendar inspectionDate) {
        this.inspectionDataLinkedList.get(index).setInspectionDate(inspectionDate);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) { this.latitude = latitude; }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getNumCritVio(int index) {
        if(numInspections==0){
            return 0;
        }
        return inspectionDataLinkedList.get(index).getNumCritVio();
    }

    public void setNumCritVio(int index, int numCritVio) {
        this.inspectionDataLinkedList.get(index).setNumCritVio(numCritVio);
    }

    public int getNumNonCritVio(int index) {
        if(numInspections==0){
            return 0;
        }
        return inspectionDataLinkedList.get(index).getNumNonCritVio();
    }

    public void setNumNonCritVio(int index, int numNonCritVio) {
        this.inspectionDataLinkedList.get(index).setNumNonCritVio(numNonCritVio);
    }

    public String getViolations(int index) {
        if(numInspections==0){
            return "no inspection";
        }
        return inspectionDataLinkedList.get(index).getViolations();
    }

    public void setViolations(int index, String violations) {
        this.inspectionDataLinkedList.get(index).setViolations(violations);
    }

    public String getTrackingKey() {
        return trackingKey;
    }

    public void setTrackingKey(String trackingKey) {
        this.trackingKey = trackingKey;
    }

    public LinkedList<InspectionData> getInspectionDataLinkedList() {
        return inspectionDataLinkedList;
    }

    public void setInspectionDataLinkedList(LinkedList<InspectionData> inspectionDataLinkedList) {
        this.inspectionDataLinkedList = inspectionDataLinkedList;
    }

    public int getNumInspections() {
        return numInspections;
    }

    public boolean isFavorited(){return isFavorited;}

    protected void toggleFavorited(){
        isFavorited = !isFavorited;
    }

}
