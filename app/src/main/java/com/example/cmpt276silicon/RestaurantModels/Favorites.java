package com.example.cmpt276silicon.RestaurantModels;

import android.app.Activity;
import android.content.SharedPreferences;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;


/*
 * Java class for adding favorite restaurant
 */

public class Favorites {
    private HashMap<String,Integer> favorites = new HashMap<>();
    private Activity activity;
    public Favorites(Activity activity){
        SharedPreferences sharedPrefs = activity.getSharedPreferences("UniversalSharedPrefKey", activity.MODE_PRIVATE);
        Set<String> prefs = sharedPrefs.getStringSet("Favorites", Collections.<String>emptySet());
        Iterator<String> itr = prefs.iterator();
        while(itr.hasNext()){
            favorites.put(itr.next(),1);
        }
        this.activity = activity;
    }

    public void toggleFavorite(String trackingKey){
        if(favorites.containsKey(trackingKey)){
            favorites.remove(trackingKey);
        }else{
            favorites.put(trackingKey,1);
        }
        Set<String> prefs = favorites.keySet();
        SharedPreferences sharedPrefs = activity.getSharedPreferences("UniversalSharedPrefKey", activity.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putStringSet("Favorites",prefs).apply();
    }

    public boolean isFavorited(String trackingKey){
        if(favorites.get(trackingKey)==null){
            return false;
        }else{
            return true;
        }
    }

}
