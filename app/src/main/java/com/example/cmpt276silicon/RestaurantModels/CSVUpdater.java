package com.example.cmpt276silicon.RestaurantModels;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/*
 * Java class for checking update every 20 hours or more since data was last updated
 * If there is new data, ask user for the permission to update data
 */

public class CSVUpdater {
    private static boolean updatingRestaurantCSV = false;
    private static boolean updatingInspectionCSV = false;

    public static boolean isUpdating(){
        return (updatingRestaurantCSV||updatingInspectionCSV);
    }

    public static boolean isUpdateRequired(Context context){
        long now  = System.currentTimeMillis();
        now = now/(1000*60*60);  //convert from milliseconds to hours
        SharedPreferences sharedPref = context.getSharedPreferences("UniversalSharedPrefKey", Context.MODE_PRIVATE);
        long last = sharedPref.getLong("HoursEpoch", 0);
        if(now-last<20){
            return false;
        }else {
            return true;
        }
    }


    public static void updateCSV(final Activity context){
        Log.d("tag0","here");
        updatingInspectionCSV =true;
        updatingRestaurantCSV=true;
        long now  = System.currentTimeMillis();
        now = now/(1000*60*60);  //convert from milliseconds to hours
        SharedPreferences sharedPref = context.getSharedPreferences("UniversalSharedPrefKey", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("HoursEpoch", now);
        editor.commit();

        final String lastRestaurntUpdate = sharedPref.getString("LastRestaurntUpdate","NULL");
        final String lastInspectionUpdate = sharedPref.getString("LastInspectionUpdate","NULL");

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("https://data.surrey.ca/api/3/action/package_show?id=restaurants").build();
        //pull correct database urls
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        JSONObject reader = new JSONObject(response.body().string());
                        String lastModified = reader.getJSONObject("result").getJSONArray("resources").getJSONObject(0).getString("last_modified");
                        Log.d("tag0",lastModified+ " "+ lastRestaurntUpdate);
                        if(lastRestaurntUpdate.compareTo(lastModified)!=0) {
                            Log.d("tag0", "new restaurants");
                            editor.putString("LastRestaurantUpdate", lastModified);

                            editor.commit();
                            String url = reader.getJSONObject("result").getJSONArray("resources").getJSONObject(0).getString("url");
                            updateCSVFromURL(url, context, true);
                        }else{
                            updatingRestaurantCSV=false;
                        }
                    } catch (Exception e) {
                        Log.d("tag1", "updateCSV: onResponse: failure");
                        e.printStackTrace();
                    }

                }else{
                    Log.d("tag1", "failure");
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("tag1", "updateCSV: OnFailure: failure");
                e.printStackTrace();
            }
        });
        request = new Request.Builder().url("https://data.surrey.ca/api/3/action/package_show?id=fraser-health-restaurant-inspection-reports").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        JSONObject reader = new JSONObject(response.body().string());
                        String lastModified = reader.getJSONObject("result").getJSONArray("resources").getJSONObject(0).getString("last_modified");
                        if(lastInspectionUpdate.compareTo(lastModified)!=0) {
                            editor.putString("LastInspectionUpdate", lastModified);
                            editor.commit();
                            String url = reader.getJSONObject("result").getJSONArray("resources").getJSONObject(0).getString("url");
                            updateCSVFromURL(url, context, false);
                        }else{
                            updatingInspectionCSV=false;
                        }
                    } catch (Exception e) {
                        Log.d("tag1", "failure");
                        e.printStackTrace();
                    }

                } else {
                    Log.d("tag1", "FAILURE else");
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("tag1", "failure");
                e.printStackTrace();
            }
        });

    }

    private static void updateCSVFromURL(String url, final Context context, final boolean flagRestaurant){

        if (url.contains("http:")) {
            url = url.substring(4);
            url = "https" + url;
        }

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String myResponse = response.body().string();
                    if(flagRestaurant) {
                        writeIntoRestaurantCSV(myResponse, context);
                    }else{
                        writeIntoInspectionCSV(myResponse,context);
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("tag1","failure");
                e.printStackTrace();
            }
        });
    }

    private static void writeIntoRestaurantCSV(String myResponse, Context context){
        File file = new File(context.getFilesDir(),"mydir");
        if(!file.exists()){
            file.mkdir();
        }
        try (FileOutputStream fos = context.openFileOutput("RestaurantCSV.csv", Context.MODE_PRIVATE)) {
            fos.write(myResponse.getBytes());
            Log.d("tag0", "done");
        }catch(Exception e){
            Log.d("tag0", "failure");
            e.printStackTrace();
        }
        updatingRestaurantCSV = false;
    }
    private static void writeIntoInspectionCSV(String myResponse, Context context){
        File file = new File(context.getFilesDir(),"mydir");
        if(!file.exists()){
            file.mkdir();
        }
        try (FileOutputStream fos = context.openFileOutput("InspectionCSV.csv", Context.MODE_PRIVATE)) {
            fos.write(myResponse.getBytes());
        }catch(Exception e){
            Log.d("tag0", "failure");
            e.printStackTrace();
        }
        updatingInspectionCSV =false;
    }

}
