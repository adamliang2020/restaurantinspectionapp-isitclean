package com.example.cmpt276silicon.RestaurantModels;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.cmpt276silicon.R;
import com.google.okhttp.OkHttpConnection;
import com.opencsv.CSVReader;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;


/*
 * Java class to getting CSV file. We also get the list of restaurant in this class
 */
public class RestaurantList {
    private LinkedList<Restaurant> restaurantList;
    private LinkedList<Restaurant> filteredList;
    private int count;
    private Context context;
    private HashMap<Integer,String> vioType;
    private Favorites favorites;
    private boolean hasFilteredList;

    private static RestaurantList instance;

    public static RestaurantList getInstance(Activity context){
        if(instance==null){
            instance = new RestaurantList(context);
            File file = new File(context.getFilesDir(),"mydir");
            if(!file.exists()){         //no download yet
                PopulateFromCSV(instance);

                Log.d("logPrint1", "kdashnfkjhdakjf");
            }else{
                while(CSVUpdater.isUpdating());
                PopulateFromInternalCSV(instance, context);
                if(instance.getCount()==0){
                    PopulateFromCSV(instance);
                }

                Log.d("logPrint2", "kdashnfkjhdakjf");
            }
            Log.d("logPrint3", String.valueOf(instance.restaurantList.size()));
        }

        return instance;
    }

    private RestaurantList(Activity context){
        restaurantList = new LinkedList<Restaurant>();
        filteredList = new LinkedList<Restaurant>();
        count =0;
        this.context = context;
        favorites = new Favorites(context);
    }

    private void clearRestaurantList() {
        restaurantList.clear();
    }

    public static boolean PopulateFromCSV(RestaurantList instance){
        Log.d("log77", "log77");
        instance.clearRestaurantList();
        CSVReader csvReaderRestaurant = new CSVReader(new InputStreamReader(instance.context.getResources().openRawResource(R.raw.restaurants_itr1)));
        CSVReader csvReaderInspection = new CSVReader(new InputStreamReader(instance.context.getResources().openRawResource(R.raw.inspectionreports_itr1)));
        return FillRestaurantList(instance,csvReaderRestaurant,csvReaderInspection,true);
    }

    public static boolean PopulateFromInternalCSV(RestaurantList instance, Context context){
        Log.d("log88", "log88");
        instance.clearRestaurantList();
        try {
            Reader targetReader = new InputStreamReader(context.openFileInput("RestaurantCSV.csv"));
            CSVReader csvReaderRestaurant = new CSVReader(targetReader);
            targetReader = new InputStreamReader(context.openFileInput("InspectionCSV.csv"));
            CSVReader csvReaderInspection = new CSVReader(targetReader);
            return FillRestaurantList(instance,csvReaderRestaurant,csvReaderInspection,false);
        }catch(IOException e) {
            Log.d("tag2", "failure: PopulateFromInternalCSV");
            e.printStackTrace();
            return false;
        }
    }

    private static boolean FillRestaurantList(RestaurantList instance, CSVReader csvReaderRestaurant, CSVReader csvReaderInspection,boolean swapped){

        try {
            CSVReader csvReader = csvReaderRestaurant;
            final HashMap<String, Restaurant> trackingKey = new HashMap<>();
            String[] rRow = csvReader.readNext(); //skip first line (titles)
            while ((rRow = csvReader.readNext()) != null ) {
                Log.d("chef1", "chef1");
                Restaurant restaurant = new Restaurant(rRow[0], rRow[1],rRow[2],rRow[3], Double.parseDouble(rRow[5]),Double.parseDouble(rRow[6]),null,0);
                if(instance.favorites.isFavorited(restaurant.getTrackingKey())){
                    restaurant.toggleFavorited();
                }
                insertRestaurant(instance, restaurant);
                trackingKey.put(rRow[0], restaurant);
                instance.count++;
            }

            Log.d("log44", String.valueOf(instance.restaurantList.size()));

            csvReader.close();
            csvReader = csvReaderInspection;
            String[] iRow = csvReader.readNext(); //skip first line (titles);
            while((iRow =  csvReader.readNext())!=null){
                Log.d("chef2", "chef2");
                if (trackingKey.containsKey(iRow[0])){
                    InspectionData inspection = new InspectionData();
                    inspection.addDate(Integer.parseInt(iRow[1]));
                    inspection.setInspectionType(iRow[2]);
                    inspection.setNumCritVio(Integer.parseInt(iRow[3]));
                    inspection.setNumNonCritVio(Integer.parseInt(iRow[4]));
                    if(swapped) {
                        inspection.setHazardRating(iRow[5]);
                        inspection.setViolations(iRow[6]);
                    }else{
                        inspection.setHazardRating(iRow[6]);
                        inspection.setViolations(iRow[5]);
                    }
                    trackingKey.get(iRow[0]).update(inspection);
                }
            }



            System.out.println();
            Log.d("log45", String.valueOf(instance.restaurantList.size()));
            return true;
        }catch (Exception e){
            Log.d("tag3", "error when filling list");
            Log.d("tag2", e.getMessage());
            return false;
        }


    }

    private static void insertRestaurant(RestaurantList instance, Restaurant restaurant){
        ListIterator<Restaurant> itr = instance.restaurantList.listIterator();
        boolean inserted = false;
        while(itr.hasNext()) {
            Restaurant compare = (Restaurant) itr.next();
            if (restaurant.getRestaurantName().compareTo(compare.getRestaurantName())<0) {
                String test = itr.previous().getRestaurantName();
                itr.add(restaurant);
                inserted = true;
                break;
            }
        }
        if(!inserted){
            itr.add(restaurant);
        }
    }

    public LinkedList<Restaurant> getList(){
        return restaurantList;
    }


    public LinkedList<Restaurant> getListContaining(String substring,int hazardRating, int minCritVio,int maxCritVio,boolean favorited, boolean usingFavorites){
        Log.d("logTest22", String.valueOf(restaurantList.size()));
        filteredList.clear();
        hasFilteredList =true;
        Log.d("tag11",substring+" "+hazardRating+" "+minCritVio+" "+maxCritVio+" "+favorited+" "+usingFavorites);
        if(hazardRating==0) {
            substring = substring.toLowerCase();
            ListIterator<Restaurant> itr = restaurantList.listIterator();
            while (itr.hasNext()) {
                Restaurant current = itr.next();
                int numCritVio = current.getNumCritVio(0);
                if(favorites.isFavorited(current.getTrackingKey())){
                    if(!current.isFavorited()) {
                        current.toggleFavorited();
                    }
                }
                if(usingFavorites) {
                    if (current.getRestaurantName().toLowerCase().contains(substring) && numCritVio <= maxCritVio && numCritVio >= minCritVio && current.isFavorited() == favorited) {
                        filteredList.add(current);
                    }
                }else{
                    if (current.getRestaurantName().toLowerCase().contains(substring) && numCritVio <= maxCritVio && numCritVio >= minCritVio) {
                        filteredList.add(current);
                    }
                }
            }
            return filteredList;
        }else{
            String HR;
            if(hazardRating==1){
                HR = "Low";
            }else if(hazardRating==2){
                HR = "Moderate";
            }else if(hazardRating==3){
                HR = "High";
            }else{
                return filteredList;
            }
            substring = substring.toLowerCase();
            ListIterator<Restaurant> itr = restaurantList.listIterator();
            while (itr.hasNext()) {
                Restaurant current = itr.next();
                int numCritVio = current.getNumCritVio(0);
                if(favorites.isFavorited(current.getTrackingKey())){
                    if(!current.isFavorited()) {
                        current.toggleFavorited();
                    }
                }
                if(usingFavorites) {
                    if (current.getRestaurantName().toLowerCase().contains(substring) && HR.compareTo(current.getHazardRating(0)) == 0 && numCritVio <= maxCritVio && numCritVio >= minCritVio && current.isFavorited() == favorited) {
                        filteredList.add(current);
                    }
                }else{
                    if (current.getRestaurantName().toLowerCase().contains(substring) && HR.compareTo(current.getHazardRating(0)) == 0 && numCritVio <= maxCritVio && numCritVio >= minCritVio) {
                        filteredList.add(current);
                    }
                }
            }
            return filteredList;
        }
    }
    public LinkedList<Restaurant> getFilteredList(){
        return filteredList;
    }

    public int getCount(){
        return count;
    }

    public void addViolationType(int key, String type){
        vioType.put(key, type);
    }

    public Restaurant getRestaurant(int index){
        if(filteredList.size()==0) {
            return restaurantList.get(index);
        }else{
            return  filteredList.get(index);
        }
    }

    public void toggleFavorites(int index){
        Restaurant restaurant;
        if(filteredList.size()==0) {
            restaurant = restaurantList.get(index);
        }else{
            restaurant = filteredList.get(index);
        }
        restaurant.toggleFavorited();
        Log.d("tag12","toggling "+restaurant.getTrackingKey());
        favorites.toggleFavorite(restaurant.getTrackingKey());
    }
    public boolean hasFilteredList(){
        return hasFilteredList;
    }

}
