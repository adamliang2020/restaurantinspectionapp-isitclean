package com.example.cmpt276silicon.RestaurantModels;

import java.util.Calendar;

//Starting project

/*
 * Java class for implementation of Inspection Data
 */

public class InspectionData {
    private Calendar inspectionDate;
    private int numCritVio;
    private int numNonCritVio;
    private String inspectionType;
    private String hazardRating;
    private String Violations;
    private String months[];

    public InspectionData(){
        numCritVio = 0;
        numNonCritVio = 0;
        hazardRating = "";
        Violations = "";
        inspectionDate = Calendar.getInstance();
        months =  new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    }

    public void addDate(int date){
        int day =  date%100;
        date = date/100; //truncates decimal
        int month = date%100;
        date = date/100; //truncates decimal
        int year = date;
        inspectionDate.set(year, month-1, day,0,0);
    }

    public String getInspectionDateString() {
        //String dateFormat;
        long days = daysSinceInspection();
        if(days<10){
            return Long.toString(days)+ " days ago";
        }else if(days<365){
            return months[inspectionDate.get(Calendar.MONTH)]+ " "+inspectionDate.get(Calendar.DAY_OF_MONTH);
        }else{
            return months[inspectionDate.get(Calendar.MONTH)]+ " "+inspectionDate.get(Calendar.YEAR);
        }
        /*dateFormat = months[inspectionDate.get(Calendar.MONTH)]+ " "+inspectionDate.get(Calendar.DAY_OF_MONTH)+", "+inspectionDate.get(Calendar.YEAR);
        return dateFormat;*/
    }

    public long daysSinceInspection(){
        long nowMil = System.currentTimeMillis();
        long diffMil = nowMil - inspectionDate.getTimeInMillis();
        long diffDays = diffMil / (24*60*60*1000);
        return diffDays;
    }


    public int getNumCritVio() {
        return numCritVio;
    }

    public void setNumCritVio(int numCritVio) {
        this.numCritVio = numCritVio;
    }

    public int getNumNonCritVio() {
        return numNonCritVio;
    }

    public void setNumNonCritVio(int numNonCritVio) {
        this.numNonCritVio = numNonCritVio;
    }

    public String getHazardRating() {
        return hazardRating;
    }

    public void setHazardRating(String hazardRating) {
        this.hazardRating = hazardRating;
    }

    public String getViolations() {
        return Violations;
    }

    public void setViolations(String violations) {
        Violations = violations;
    }

    public Calendar getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(Calendar inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getInspectionType() {
        return inspectionType;
    }

    public void setInspectionType(String inspectionType) {
        this.inspectionType = inspectionType;
    }
}
