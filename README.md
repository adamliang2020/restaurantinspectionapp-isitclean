Group Project 
Inspection data is sourced from the fraser health restaurant inspection database

<br/>

## Iteration 1 User Stories
 - Display list of all restaurants
 - Display details of single restaurant
 - Display details of single inspection
 
<br/>

<br/>

## Team Members + Roles (for Project Iteration 1)
Rotating Roles
 - **[Dr. Brian Fraser](mailto:bfraser@sfu.ca)** *as the Instructor*
 - **[Chintana Prabhu](mailto:cprabhu@sfu.ca)** *as Teaching Assistant*
 - **[Tirth Patel](mailto:tirthp@sfu.ca)** *as Teaching Assistant*
 - **[Pranjal Keshari](mailto:pkeshari@sfu.ca)** *as the Repo Manager*
 - **[Adam Liang](mailto:ala193@sfu.ca)** *as the Product Owner*
 - **[Yusuf Saleem](mailto:ysaleem@sfu.ca)** *as a Scrum Master*
 - **[Dipansh](mailto:ddipansh@sfu.ca)** *as the Team Member*
 
<br/>


<br/>

## Iteration 2 User Stories
 - Get updated data
 - Map
 - Custom images / icons
 - Back-button behaviour

<br/>

<br/>

## Team Members + Roles (for Project Iteration 2)
 - **[Dr. Brian Fraser](mailto:bfraser@sfu.ca)** *as the Instructor*
 - **[Chintana Prabhu](mailto:cprabhu@sfu.ca)** *as Teaching Assistant*
 - **[Tirth Patel](mailto:tirthp@sfu.ca)** *as Teaching Assistant*
 - **[Pranjal Keshari](mailto:pkeshari@sfu.ca)** *as the Product Owner*
 - **[Adam Liang](mailto:ala193@sfu.ca)** *as the Scrum Master*
 - **[Yusuf Saleem](mailto:ysaleem@sfu.ca)** *as a Repo Manager*
 - **[Dipansh](mailto:ddipansh@sfu.ca)** *as the Team Member*

<br/>

<br/>

## Iteration 3 User Stories
Please note that after updating it takes a second for the fragment that shows which favorites have been updated to show up. All the user stories are complete
 - Search / Filter
 - Favourites
 - Internationalize
 
<br/>

<br/>

## Team Members + Roles (for Project Iteration 3)
 - **[Dr. Brian Fraser](mailto:bfraser@sfu.ca)** *as the Instructor*
 - **[Chintana Prabhu](mailto:cprabhu@sfu.ca)** *as Teaching Assistant*
 - **[Tirth Patel](mailto:tirthp@sfu.ca)** *as Teaching Assistant*
 - **[Pranjal Keshari](mailto:pkeshari@sfu.ca)** *as the Team Member*
 - **[Adam Liang](mailto:ala193@sfu.ca)** *as the Repo Manager*
 - **[Yusuf Saleem](mailto:ysaleem@sfu.ca)** *as a Product Owner*
 - **[Dipansh](mailto:ddipansh@sfu.ca)** *as the Scrum Master*
 
<br/>

## Online Communication Channel
**Discord:** [Click here to join the channel](https://discord.gg/bq3Tew)

<br/>

## Project Miscellaneous Storage
**Google Drive:** [Click here to view related files](https://drive.google.com/drive/folders/1h0L4v99mfNJAMrEX024YhEslKNufcQlU)

<br/>

## In Person Meetings
After CMPT 276 class *(Location: Room number 3016, 4016, and Computer Lab)*